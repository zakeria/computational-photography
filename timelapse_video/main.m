function main


% Script configurations
data_directory = 'data/'; % make same as preprocess dir
file_prefix = 'test';

begin_frame_digits = 291; % Begin frame number on the file.
end_frame_digits = 4121;  % Begin frame number on the file.
vlc_skip_frames = true;   % Vlc scene filter frame skipping.
vlc_skip_size = 10;       % Vlc scene filter frame Skip size.
digits = 5; 

frames = load_frames(data_directory, file_prefix, begin_frame_digits, end_frame_digits, digits,'jpg', vlc_skip_frames, vlc_skip_size);
 
mean_range = 10;  
skip_thresh = 5;    

% Automate threshold selection
%distances = get_distances(frames, mean_range); 
%sample_threshold = mean(distances);   

sample_threshold = 0.05; 
[sampled_frames, distances] = sample_frames(frames, mean_range, sample_threshold, skip_thresh); % Sample frames above mean distance.

% Play the resulting time-lapse video.
implay(sampled_frames);
 
% Plot the resulting SSDs.
figure; 
plot(distances);
hold on;
for i = 1:length(distances)
    if(distances(i) > sample_threshold)
       plot(i, distances(i), 'ro');        
    end
end
hold off;
end

% Compute the SSDs for each frame in the input video. According to
% some givne image_ranges size.
function [distances] = get_distances(frames, mean_range)
frame_count = size(frames,4);

[rows,cols,~,~] = size(frames);
norm_const = rows * cols;

for i=1:frame_count
    current_frame = frames(:,:,:,i);

    image_ranges = get_image_ranges(frames, i, mean_range);
    
    if(isempty(image_ranges))
        continue;
    end
    
    mean_frame = mean_intensity_range(frames, image_ranges);
     
    distance = sqrt((current_frame - mean_frame).^2);    
    distance = sum(distance(:));
    distance = distance / norm_const; 
     
    distances(i) = distance; 
end 
end

% Sample the input video according to the sample_threshold.
function [samples, distances] = sample_frames(frames, mean_range, sample_threshold, skip_thresh)
frame_count = size(frames,4);

[X,Y,~,~] = size(frames);

output_index = 1;

norm_const = X * Y; 
  
skip_frame = 0;    

for i=1:frame_count  
    current_frame = frames(:,:,:,i);
     
    image_ranges = get_image_ranges(frames, i, mean_range); 
    
    if(isempty(image_ranges))
        continue;
    end
    mean_frame = mean_intensity_range(frames, image_ranges);
     
    distance = sqrt((current_frame - mean_frame).^2);    
    distance = sum(distance(:));
    distance = distance / norm_const; 
        
    if(distance < sample_threshold)
        if(skip_frame == skip_thresh)
            samples(:,:,:, output_index) = current_frame;
            output_index = output_index + 1;
            skip_frame = 0; 
        end
        skip_frame = skip_frame + 1;
    else
        samples(:,:,:, output_index) = current_frame;
        output_index = output_index + 1;
    end
        distances(i) = distance;
end
end

% Get an image at a given index. 
function [res_idx] = get_image(mov, idx)
[X,Y,D, Z] = size(mov);
if( idx <= 0 || idx > Z)
    res_idx = -1;
else
    res_idx = idx;
end
end

% Obtain the indices for a range of images, that surround the given image
% index.
function [res_indices] = get_image_ranges(mov, current_idx, range_size)

assert(range_size >= 1,'image_ranges must be positive')

res_indices = zeros(1,range_size);
range_p = current_idx - range_size:current_idx - 1;
 
for i=1:length(range_p)
    res_indices(i) = get_image(mov, range_p(i));
end
res_indices = res_indices(res_indices~=current_idx); 
res_indices = res_indices(res_indices~=-1);
res_indices = res_indices(res_indices~=0);
end
 
% Get mean of images for a given image_ranges
function [res_img] = mean_intensity_range(mov, image_ranges) 
   
if(length(image_ranges) == 1)
    res_img = mov(:,:,:,image_ranges);
    return;
end 
for i = 1 : length(image_ranges)
    if(i == 1)
        res_img = mov(:,:,:,image_ranges(1));
        continue;
    end
    res_img = res_img + mov(:,:,:,image_ranges(i)); 
end
res_img = res_img ./ length(image_ranges);
end