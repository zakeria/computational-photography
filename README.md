# Computational Photography and Capture

This repository contains examples of computational photography and capture algorithms, such as old film restoration, time lapse video generation and pose reconstruction


Computational Photography is an emerging new field created by the convergence of photography, computer graphics and computer vision. Its role is to overcome the limitations of the traditional camera by using computational techniques to produce a richer, perceptually meaningful representation of our visual world. 