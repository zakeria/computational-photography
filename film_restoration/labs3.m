function output = labs3(path, prefix, first, last, digits, suffix)

%
% Read a sequence of images and correct the film defects. This is the file 
% you have to fill for the coursework. Do not change the function 
% declaration, keep this skeleton. You are advised to create subfunctions.
% 
% Arguments:
%
% path: path of the files
% prefix: prefix of the filename
% first: first frame
% last: last frame
% digits: number of digits of the frame number
% suffix: suffix of the filename
%
% This should generate corrected images named [path]/corrected_[prefix][number].png
%
% Example:
%
% mov = labs3('../images','myimage', 0, 10, 4, 'png')
%   -> that will load and correct images from '../images/myimage0000.png' to '../images/myimage0010.png'
%   -> and export '../images/corrected_myimage0000.png' to '../images/corrected_myimage0010.png'
%
 
mov = load_sequence(path, prefix, first, last, digits, suffix);
mov = im2double(mov);
frame_count = size(mov, 3);

scene_cut_thresh = 0.8;
motion_thresh = 0.1;
shake_thresh = 0.05;

[cut_indices, annotated_mov] = get_scenecuts(mov, frame_count, scene_cut_thresh);
[flicker_correction] = correct_intensity_flickers(annotated_mov, frame_count, cut_indices, 10);
[blotch_correction] = remove_blotches(flicker_correction, frame_count, motion_thresh);
[edge_correction] = correct_vertical_artefacts(blotch_correction, frame_count);
[shake_correction] = correct_shake(edge_correction, frame_count, cut_indices, shake_thresh);

output = shake_correction;
end