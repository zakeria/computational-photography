% Get mean of images for a given image_ranges
function [res_img] = mean_intensity_range(mov, image_ranges) 
   
if(length(image_ranges) == 1)
    res_img = mov(:,:,image_ranges);
    return;
end 

for i = 1 : length(image_ranges)
    if(i == 1)
        res_img = mov(:,:,image_ranges(1));
        continue;
    end
    res_img = res_img + mov(:,:,image_ranges(i));
end
res_img = res_img ./ length(image_ranges);
end

