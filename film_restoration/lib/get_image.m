function [res_idx] = get_image(mov, idx)
[X,Y,Z] = size(mov);
if( idx <= 0 || idx > Z)
    res_idx = -1;
else
    res_idx = idx;
end
end