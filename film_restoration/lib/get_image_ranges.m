function [res_indices] = get_image_ranges(mov, current_idx, range_size)

assert(range_size >= 1,'image_ranges must be positive')

res_indices = zeros(1,range_size);
range_p = current_idx-round(range_size/2):current_idx+round(range_size/2);
range_p = range_p(range_p~=current_idx);

for i=1:length(range_p)
    res_indices(i) = get_image(mov, range_p(i));
end

res_indices = res_indices(res_indices~=-1);