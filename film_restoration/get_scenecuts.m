function [cut_indices, annotated_mov] = get_scenecuts(mov, frame_count, thresh)
cut_indices = zeros(1, frame_count); cut_idx = 1;
annotated_mov = mov;

for frame_idx = 2:frame_count
    current_frame = mov(:,:,frame_idx);
    prevous_frame = mov(:,:,frame_idx-1);
    
    % SAD similarity between frames
    sum_abs_diff = imabsdiff(current_frame, prevous_frame);
    sum_abs_diff = sum(sum_abs_diff(:));
    
    cut_indices(cut_idx) = sum_abs_diff; 
    cut_idx = cut_idx + 1;
end

% Normalise our data between [0,1].
min_val = min(cut_indices);
max_val = max(cut_indices);
cut_indices = (cut_indices - min_val) / (max_val - min_val);

cut_indices = find(cut_indices > thresh);
for i=1:length(cut_indices)
   cut_frame = mov(:,:, cut_indices(i));
   annotated_frame = insertText(cut_frame, [0,0], 'Scene cut detected', 'FontSize', 22);
   annotated_mov(:,:,cut_indices(i)) = rgb2gray(annotated_frame);
end