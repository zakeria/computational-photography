function [corrected_mov] = remove_blotches(mov, frame_count, motion_thresh)

for frame_idx = 1:frame_count
    current_frame = mov(:,:,frame_idx);
       
    next_frame_idx = get_image(mov, frame_idx + 1); 
    if(next_frame_idx == -1) % last frame
         next_frame_idx = get_image(mov, frame_idx - 1);
    end
    
    next_frame = mov(:,:, next_frame_idx);

    % Identify and create a mask of motion objects
    frame_motion = imabsdiff(current_frame, next_frame);
    frame_motion(frame_motion < motion_thresh) = 0;
    frame_motion(frame_motion > motion_thresh) = 1;
   
    % Dialate 
    se = strel('disk', 5);
    blotch_frame = imdilate(frame_motion, se);         
           
    % Compute average of surrounding frames for detected blotch region.
    images_range = get_image_ranges(mov, frame_idx, 5);   
    mean_image = mean_intensity_range(mov, images_range); 
   
    [rows, cols] = ind2sub(size(blotch_frame), find(blotch_frame));
    fixed_frame = current_frame;
    for x = 1:length(rows)
        fixed_frame(rows(x), cols(x)) = mean_image(rows(x), cols(x));        
    end
    corrected_mov(:,:,frame_idx) = fixed_frame;
end
end

