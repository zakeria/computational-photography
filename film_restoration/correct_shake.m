function [corrected_mov] = correct_shake(mov, frame_count, cut_indices, thresh)
for frame_idx = 2:frame_count         
    current_frame = mov(:,:,frame_idx);
    
     if(ismember(frame_idx, cut_indices))
        continue;
     end
    
    prev_frame_idx = get_image(mov, frame_idx - 1);    
    
    prev_frame = mov(:,:,prev_frame_idx);
   
    points_current = detectFASTFeatures(current_frame, 'MinContrast', thresh);
    points_prev = detectFASTFeatures(prev_frame, 'MinContrast', thresh);
        
    [features_current, points_current] = extractFeatures(current_frame, points_current);
    [features_prev, points_prev] = extractFeatures(prev_frame, points_prev);
    
    index_pairs = matchFeatures(features_current, features_prev);
    points_current = points_current(index_pairs(:, 1), :);
    points_prev = points_prev(index_pairs(:, 2), :); 
              
    % RANSAC algorithm
    [affine_transformation, ~, ~, status_code] = estimateGeometricTransform(points_prev, points_current, 'affine');
    if(status_code == 1) % inputs do not contain enough points, skip
        continue;
    end
    
    fixed_frame = imwarp(prev_frame, affine_transformation, 'OutputView', imref2d(size(prev_frame)));   
    corrected_mov(:,:,frame_idx) = fixed_frame;
end
end
