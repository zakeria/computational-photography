function [corrected_mov] = correct_intensity_flickers(mov, frame_count, cut_indices, range_indices)

corrected_mov = mov;
for frame_idx = 1:frame_count    

    current_frame = mov(:,:,frame_idx);          
    images_range = get_image_ranges(mov, frame_idx, range_indices);
    
    mean_image = mean_intensity_range(mov, images_range);  
    
    corrected_mov(:,:, frame_idx) = histeq(current_frame, imhist(mean_image));
end