function [corrected_mov] = correct_vertical_artefacts(mov, frame_count)

corrected_mov = mov;

[X,Y,Z] = size(mov);

for frame_idx = 1:frame_count
    current_frame = mov(:,:,frame_idx);
    for x = 1:X
        current_frame(x,:) = medfilt1(current_frame(x,:), 7);
    end      
    corrected_mov(:,:,frame_idx) = current_frame;
end
end
